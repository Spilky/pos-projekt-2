CC=gcc
CFLAGS=-std=c99 -Wall -pedantic -pthread

proj02: proj02.c
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -f *.o proj02
