//
// Created by David Spilka (xspilk00) on 27.4.16.
//

#define _POSIX_C_SOURCE 199506L

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED 1

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>

#define MAX_LINE_LENGTH 513

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

pid_t pid = 1;
pid_t waitFor = 0;

struct sigaction sigactGlobal;

int interrupt = 0;
int end = 0;
int reading = 1;
char buffer[MAX_LINE_LENGTH];

int isAmp;
int isInFile;
int isOutFile;
char * inFile;
char * outFile;
int paramsCount;
char * params[MAX_LINE_LENGTH];

void chldCatcher(int sig)
{
    if (sig == SIGCHLD) {
        int res = 0;
        pid_t pid = wait(&res);
        if (pid == waitFor) {
            waitFor = 0;
        } else if (pid != -1) {
            printf("\nDone [%d]\n", pid);
            printf("~$ ");
            fflush(stdout);
        }
    }
}

void intCatcher(int sig)
{
    if (sig == SIGINT) {
        if (pid == 0) {
            exit(EXIT_SUCCESS);
        }
        else {
            printf("\n~$ ");
            fflush(stdout);
            interrupt = 1;
        }
    }
}

void * threadInputProcess()
{
    int lineLength;
    char c;

    printf("~$ ");
    fflush(stdout);

    while (1) {
        memset(buffer, '\0', MAX_LINE_LENGTH);

        lineLength = read(STDIN_FILENO, &buffer, MAX_LINE_LENGTH);

        if (lineLength == -1) {
            perror("Error: read() for SIGCHLD");
            exit(EXIT_FAILURE);
            continue;
        } else if (lineLength == 0) {
            end = 1;
            pthread_cond_signal(&cond);
            break;
        } else if (lineLength > MAX_LINE_LENGTH - 1) {
            c = buffer[MAX_LINE_LENGTH - 1];
            perror("Error: Input is too long");
            while (c != '\n') {
                read(STDIN_FILENO, &c, 1);
            }

            printf("~$ ");
            fflush(stdout);

            continue;
        } else {
            int i, emptyString = 1;

            for (i = 0; i < lineLength; i++) {
                if (!isspace(buffer[i])) {
                    emptyString = 0;
                }
            }

            if (emptyString) {
                printf("~$ ");
                fflush(stdout);

                continue;
            }

            buffer[lineLength - 1] = '\0';
        }

        reading = 0;
        interrupt = 0;
        pthread_cond_signal(&cond);

        pthread_mutex_lock(&mutex);
        while (!reading) {
            pthread_cond_wait(&cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        if (end) {
            break;
        }

        if (!interrupt)
        {
            printf("~$ ");
            fflush(stdout);;
        }
    }

    pthread_exit(NULL);
}

char * getWordFromBuffer(int start)
{
    int i, wordStart = -1;

    for (i = start; buffer[i] != '\0'; i++) {
        if (isspace(buffer[i])) {
            if (wordStart != -1) {
                buffer[i] = '\0';
                break;
            } else {
                continue;
            }
        }

        if (wordStart == -1) {
            wordStart = i;
        }
    }

    return &buffer[wordStart];
}

int getCharIndex(char c)
{
    char *ptr;
    int index;

    ptr = strchr(buffer, c);
    if (ptr) {
        index = ptr - buffer;
        return index;
    }

    return -1;
}

void parseCommand()
{
    int i;
    char * parameter;

    memset(params, '\0', MAX_LINE_LENGTH);

    paramsCount = 0;

    int ampIndex = getCharIndex('&');
    int leftIndex = getCharIndex('<');
    int rightIndex = getCharIndex('>');

    for (i = 0; buffer[i] != '\0'; i++) {
        if (i == ampIndex || i == leftIndex || i == rightIndex) {
            break;
        }

        if (isspace(buffer[i])) {
            continue;
        }

        parameter = getWordFromBuffer(i);
        i += strlen(parameter);
        params[paramsCount] = parameter;
        paramsCount++;
    }

    if (ampIndex == -1) {
        isAmp = 0;
    } else {
        isAmp = 1;
    }

    if (leftIndex == -1) {
        isInFile = 0;
    } else {
        isInFile = 1;
        inFile = getWordFromBuffer(leftIndex + 1);
    }

    if (rightIndex == -1) {
        isOutFile = 0;
    } else {
        isOutFile = 1;
        outFile = getWordFromBuffer(rightIndex + 1);
    }
}

int childProcess() {
    int inFileDesc;
    int outFileDesc;
    int returnRes;

    sigset_t setEmpty;
    sigset_t setFull;

    sigemptyset(&setEmpty);
    sigfillset(&setFull);
    sigprocmask(SIG_UNBLOCK, &setFull, NULL);

    if (isInFile) {
        inFileDesc = open(inFile, O_RDONLY);

        if (inFileDesc == -1) {
            perror("Error: open() for input file\n");
            return EXIT_FAILURE;
        }

        close(STDIN_FILENO);
        dup2(inFileDesc, STDIN_FILENO);
    }

    if (isOutFile) {
        outFileDesc = open(outFile, O_CREAT | O_TRUNC | O_WRONLY,
                           S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

        if (outFileDesc == -1) {
            perror("Error: open() for output file\n");
            return EXIT_FAILURE;
        }

        close(STDOUT_FILENO);
        dup2(outFileDesc, STDOUT_FILENO);
    }

    if (isAmp) {
        sigset_t setint;
        sigemptyset(&setint);
        sigaddset(&setint, SIGINT);

        sigprocmask(SIG_BLOCK, &setint, NULL);

        sigactGlobal.sa_handler = SIG_DFL;
        if (sigaction(SIGCHLD, &sigactGlobal, NULL) == -1)
        {
            perror("Error: sigaction() for SIGCHLD\n");
            return EXIT_FAILURE;
        }
    }

    returnRes = execvp(params[0], params);
    if (returnRes == -1) {
        perror("Error: execvp()\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void parentProcess(pid_t childPid)
{
    if (!isAmp) {
        waitFor = childPid;
        sigset_t setEmpty;
        sigemptyset(&setEmpty);
        while(waitFor) {
            sigsuspend(&setEmpty);
        }
    }
}

void * threadCommandProcess()
{
    sigset_t setFull;
    sigset_t setEmpty;

    sigfillset(&setFull);
    sigemptyset(&setEmpty);

    while (1) {
        pthread_mutex_lock(&mutex);
        while (reading) {
            pthread_cond_wait(&cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        if (end) {
            break;
        }

        parseCommand();

        if (!strcmp(params[0], "exit")) {
            //deleteCommand();
            end = 1;
            reading = 1;
            pthread_cond_signal(&cond);
            break;
        }

        pthread_sigmask(SIG_SETMASK, &setFull, NULL);

        pid = fork();
        if (pid == -1) {
            perror("Error: fork()\n");
        } else if (pid == 0) {
            // Child process
            exit(childProcess());
        } else {
            // Parent process
            parentProcess(pid);
        }

        pthread_sigmask(SIG_SETMASK, &setEmpty, NULL);

        reading = 1;
        pthread_cond_signal(&cond);
        fflush(stdout);
    }

    pthread_exit(NULL);
}

int main(void)
{
    struct sigaction sigact;

    sigset_t setInt;
    sigset_t setChld;

    pthread_attr_t attr;
    pthread_t threatInput;
    pthread_t threatCommand;

    void * resultInput;
    void * resultCommand;

    sigemptyset(&setInt);
    sigemptyset(&setChld);

    sigaddset(&setInt, SIGINT);
    sigaddset(&setChld, SIGCHLD);

    sigprocmask(SIG_BLOCK, &setInt, NULL);

    sigactGlobal.sa_handler = intCatcher;
    sigemptyset(&sigactGlobal.sa_mask);
    sigactGlobal.sa_flags = 0;

    if (sigaction(SIGINT, &sigactGlobal, NULL) == -1) {
        perror("Error: sigaction() for SIGINT\n");
        exit(EXIT_FAILURE);
    }

    sigact.sa_handler = chldCatcher;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;

    if (sigaction(SIGCHLD, &sigact, NULL) == -1) {
        perror("Error: sigaction() for SIGCHLD");
        exit(EXIT_FAILURE);
    }

    sigprocmask(SIG_UNBLOCK, &setInt, NULL);

    if (pthread_attr_init(&attr) != 0) {
        perror("Error: pthread_attr_init()");
        exit(EXIT_FAILURE);
    }

    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE) != 0) {
        perror("Error: pthread_attr_setdetachstate()");
        exit(EXIT_FAILURE);
    }

    if (pthread_create(&threatInput, &attr, &threadInputProcess, NULL) != 0){
        perror("Error: pthread_create() for input threat");
        exit(EXIT_FAILURE);
    }
    if (pthread_create(&threatCommand, &attr, &threadCommandProcess, NULL) != 0){
        perror("Error: pthread_create() for command threat");
        exit(EXIT_FAILURE);
    }

    if (pthread_join(threatInput, &resultInput) != 0) {
        perror("Error: pthread_join() for input threat");
        exit(EXIT_FAILURE);
    }
    if (pthread_join(threatCommand, &resultCommand) != 0) {
        perror("Error: pthread_join() for command threat");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
